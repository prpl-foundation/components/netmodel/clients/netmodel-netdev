/****************************************************************************
**
** SPDX-License-Identifier: BSD-2-Clause-Patent
**
** SPDX-FileCopyrightText: Copyright (c) 2022 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
#include <ctype.h>

#include <amxc/amxc.h>
#include <amxc/amxc_macros.h>
#include <amxs/amxs.h>
#include <amxm/amxm.h>
#include <debug/sahtrace.h>
#include <debug/sahtrace_macros.h>
#include <netmodel/client.h>
#include <netmodel/mib.h>
#include "mib_netdev.h"

#define ME "netdev_mib"
#define MOD "netdev"
#define UNUSED __attribute__((unused))
#define string_empty(x) ((x == NULL) || (*x == '\0'))

static void update_flag(const char* netmodel_intf, const char* flag, bool set) {
    amxc_var_t args;
    amxc_var_t ret;
    const char* function = set ? "setFlag" : "clearFlag";

    amxc_var_init(&args);
    amxc_var_init(&ret);

    amxc_var_set_type(&args, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &args, "flag", flag);

    if(AMXB_STATUS_OK != amxb_call(netmodel_get_amxb_bus(), netmodel_intf, function, &args, &ret, 5)) {
        SAH_TRACEZ_ERROR(ME, "Failed to call %s%s(flag='%s')", netmodel_intf, function, flag);
        goto exit;
    }

exit:
    amxc_var_clean(&args);
    amxc_var_clean(&ret);
}

static amxs_status_t status_action_cb(const amxs_sync_entry_t* sync_entry UNUSED,
                                      amxs_sync_direction_t direction,
                                      amxc_var_t* data,
                                      void* priv UNUSED) {
    const char* netmodel_intf = GET_CHAR(data, "path");
    const char* status = GETP_CHAR(data, "parameters.State");
    bool is_up = false;

    when_false((direction == amxs_sync_a_to_b), exit);

    is_up = !string_empty(status) && (strcmp(status, "up") == 0);

    update_flag(netmodel_intf, "netdev-up", is_up);
exit:
    return amxs_status_ok;
}

static amxs_status_t index_action_cb(const amxs_sync_entry_t* sync_entry,
                                     amxs_sync_direction_t direction,
                                     amxc_var_t* data,
                                     void* priv) {
    const char* netmodel_intf = GET_CHAR(data, "path");
    int32_t index = GETP_INT32(data, "parameters.NetDevIndex");

    // Execute standard copy action
    amxs_sync_param_copy_action_cb(sync_entry, direction, data, priv);

    when_false((direction == amxs_sync_a_to_b), exit);

    update_flag(netmodel_intf, "netdev-bound", index > 0);
exit:
    return amxs_status_ok;
}

static amxs_status_t add_parameters(amxs_sync_ctx_t* ctx) {
    amxs_status_t status = amxs_status_unknown_error;

    status = amxs_sync_ctx_add_new_copy_param(ctx, "Name", "NetDevName", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_param(ctx, "Index", "NetDevIndex", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, index_action_cb, ctx);
    status |= amxs_sync_ctx_add_new_copy_param(ctx, "Flags", "NetDevFlags", AMXS_SYNC_DEFAULT);
    status |= amxs_sync_ctx_add_new_param(ctx, "State", "State", AMXS_SYNC_DEFAULT, amxs_sync_param_copy_trans_cb, status_action_cb, ctx);

    return status;
}

static char* find_netdev_name_by_interface_path(const char* interface_path) {
    amxc_var_t ret;
    amxc_string_t search_path_str;
    const char* search_path = NULL;
    const char* temp = NULL;
    char* name = NULL;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_string_init(&search_path_str, 0);

    amxc_string_setf(&search_path_str, "NetModel.Intf.[InterfacePath == '%s'].", interface_path);
    search_path = amxc_string_get(&search_path_str, 0);
    rv = amxb_get(netmodel_get_amxb_bus(), search_path, 0, &ret, 5);
    if(AMXB_STATUS_OK != rv) {
        SAH_TRACEZ_ERROR(ME, "No instance matching search path %s, reason: %d", search_path, rv);
        goto exit;
    }

    temp = GETP_CHAR(&ret, "0.0.Name");
    if(string_empty(temp)) {
        SAH_TRACEZ_INFO(ME, "Instance %s has empty NetDevName parameter", amxc_var_key(GETP_ARG(&ret, "0.0")));
        goto exit;
    }

    name = strdup(temp);

exit:
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path_str);
    return name;
}

static char* find_instance_of_netdev(amxb_bus_ctx_t* bus, const char* interface_path) {
    amxc_var_t ret;
    amxc_string_t search_path_str;
    const char* search_path = NULL;
    const char* temp = NULL;
    char* netdev_name = NULL;
    char* instance = NULL;
    int rv = -1;

    amxc_var_init(&ret);
    amxc_string_init(&search_path_str, 0);

    if(string_empty(interface_path)) {
        SAH_TRACEZ_ERROR(ME, "Missing interface path");
        goto exit;
    }

    netdev_name = find_netdev_name_by_interface_path(interface_path);
    when_str_empty(netdev_name, exit);

    amxc_string_setf(&search_path_str, "NetDev.Link.[Name == '%s'].", netdev_name);
    search_path = amxc_string_get(&search_path_str, 0);
    rv = amxb_get(bus, search_path, 0, &ret, 5);
    if(AMXB_STATUS_OK != rv) {
        SAH_TRACEZ_ERROR(ME, "No instance matching search path %s, reason: %d", search_path, rv);
        goto exit;
    }

    temp = amxc_var_key(GETP_ARG(&ret, "0.0"));
    if(string_empty(temp)) {
        SAH_TRACEZ_ERROR(ME, "Instance %s not found", search_path);
        goto exit;
    }

    instance = strdup(temp);

exit:
    free(netdev_name);
    amxc_var_clean(&ret);
    amxc_string_clean(&search_path_str);
    return instance;
}

static void create_netdev_subscription(UNUSED amxb_bus_ctx_t* bus, const char* interface_path, amxc_string_t* object, amxc_string_t* expression) {
    char* netdev_name = NULL;

    netdev_name = find_netdev_name_by_interface_path(interface_path);
    when_str_empty(netdev_name, exit);

    amxc_string_appendf(expression, " && path == 'NetDev.Link.' && parameters.Name == '%s'", netdev_name);
    amxc_string_set(object, "NetDev.Link.");

exit:
    if(string_empty(netdev_name)) {
        SAH_TRACEZ_WARNING(ME, "Won't be able to change sync object_a[%s]", interface_path);
    }
    free(netdev_name);
}

static void reset_parameters(amxs_sync_ctx_t* ctx) {
    const char* netmodel_intf = ctx->b;

    amxc_var_t values;
    amxc_var_t ret;

    amxc_var_init(&ret);
    amxc_var_init(&values);

    when_str_empty_trace(netmodel_intf, exit, ERROR, "Netmodel interface path is empty");

    amxc_var_set_type(&values, AMXC_VAR_ID_HTABLE);
    amxc_var_add_key(cstring_t, &values, "NetDevFlags", "");
    amxc_var_add_key(uint32_t, &values, "NetDevIndex", 0);
    amxc_var_add_key(cstring_t, &values, "NetDevName", "");

    if(AMXB_STATUS_OK != amxb_set(netmodel_get_amxb_bus(), netmodel_intf, &values, &ret, 3)) {
        SAH_TRACEZ_ERROR(ME, "Failed to reset parameters on interface path %s", netmodel_intf);
        goto exit;
    }
    update_flag(netmodel_intf, "netdev-bound netdev-up", false);

exit:
    amxc_var_clean(&values);
    amxc_var_clean(&ret);
}

static netmodel_mib_t mib = {
    .name = ME,
    .root = "NetDev.",
    .flags = NETMODEL_MIB_DONT_LINK_INTERFACES |
        NETMODEL_MIB_ONLY_MY_PARAMETERS,
    .add_sync_parameters = add_parameters,
    .find_instance_to_sync = find_instance_of_netdev,
    .create_subscription_for_object_a = create_netdev_subscription,
    .reset_sync_parameters = reset_parameters,
};

int mib_added(UNUSED const char* const function_name,
              amxc_var_t* args,
              UNUSED amxc_var_t* ret) {
    netmodel_subscribe(args, &mib);

    return 0;
}

int mib_removed(UNUSED const char* const function_name,
                amxc_var_t* args,
                UNUSED amxc_var_t* ret) {
    netmodel_unsubscribe(args, &mib);
    update_flag(GET_CHAR(args, "object"), "netdev-up", false);

    return 0;
}

static AMXM_CONSTRUCTOR mib_init(void) {
    amxm_shared_object_t* so = amxm_so_get_current();
    amxm_module_t* mod = NULL;

    SAH_TRACEZ_INFO(ME, "Mib init triggered");

    amxm_module_register(&mod, so, MOD);
    amxm_module_add_function(mod, "mib-added", mib_added);
    amxm_module_add_function(mod, "mib-removed", mib_removed);

    netmodel_initialize();

    return 0;
}

static AMXM_DESTRUCTOR mib_clean(void) {
    SAH_TRACEZ_INFO(ME, "Mib clean triggered");

    netmodel_cleanup();

    return 0;
}
