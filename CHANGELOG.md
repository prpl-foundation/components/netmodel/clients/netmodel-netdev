# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]


## Release v1.0.4 - 2023-03-21(14:26:15 +0000)

### Fixes

- [NetModel] NetDev information in netmodel ppp-wan interface is not cleared when ppp connection stops

## Release v1.0.3 - 2022-05-31(07:33:12 +0000)

### Fixes

- [NetModel] NetDevName doesn't sync if the interface does not yet exist during find_instance_to_sync_callback

## Release v1.0.2 - 2022-04-26(06:58:44 +0000)

### Fixes

- netdev-up flag on vlan-vlan201 is gone after switching WANManager's WANMode twice

## Release v1.0.1 - 2022-04-13(11:50:41 +0000)

### Changes

- Vlan Interfaces are not up after creation

## Release v1.0.0 - 2022-02-28(10:54:03 +0000)

### Breaking

- Use amxm to start/stop syncing netmodel clients

## Release v0.1.0 - 2022-02-07(08:45:57 +0000)

### New

- implement netdev client and netdev mib

